Before you can add an arena for a minigame, you need to have a map. 

======================================================

Making the map

======================================================

~~***We do not make maps on the server***~~ eh, we do :P

Download the game (see "updating the game"), and install it in your local copy of minetest.

Make the arena on your own computer, you can use WorldEdit and Terraform to speed things up tremendously.

Make sure that the arena is well decorated and has a theme.

Also make sure that it is unescapable. Use server_manager:barrier nodes to make it unescapable, make it 2 nodes thick at a minimum.

Use WorldEdit to save a .mts file of the map

set pos1 (`//1`) and pos2 (`//2`) to completely surround the arena with the Worldedit area. Then Use the command

`//mtschemcreate <arena name>` replacing the name to whatever you want to name it

Then, in your local minetest installation, enter the world folder, and look in the subfolder "schems"

find the .mts file that you just made.


======================================================

Uploading an arena to the server

======================================================

Either a player gives you an arena, or you have made one. 

add the arena to the repo:

https://gitlab.com/arcadeemulationserver/schems

Also edit the readme file in that repo to indicate the author and the license of the schematic. Do your best to get the author to tell you the license. If you made it, choose an appropriate license (NOT CC - NON-COMMERCIAL, Please!!!)


then on the server, 

```
cd /home/minetest/minetest4/worlds/pineapple_server_3/schems

git pull
```

the arena should now be in the schems folder.

There is no need to restart the server, you can access the arena schem immediately. 
 

Log on to the server, find a spot that is suitable (not near anything !!!!, like 200 nodes away at a minimum!), set pos1 of WorldEdit, `//pos1`, and then place the schematic there `//mtschemplace <name>` (name does not have the .mts on the end)



======================================================

Setting up arenas 

======================================================


Setting up an arena is a tricky business. Be very careful when you do it, and practice offline on your own computer first!

When you set points of arenas where the arena will reset an area, it is very easy to get the locations wrong. When you have to type it in from the settings menu, for some reason, arena_lib puts "y" first, then "x", and then "z" ! (`{y = 0, x = 0, z = 0}`) But all other places you see a coordinate the order is x,y,z!!!! YOu can easily get it worng! be careful!

IF YOU MESS IT UP, IT CAN CRASH THE SERVER AND CORRUPT THE WORLD IRREPARABLY! That is why we have 2 world backups BTW.

Before setting up an arena, you should be familiar with the minigame and if you are not familiar with it, practice offline. As you set it up, be very careful to get the correct locations of points. Double and triple check it before you enable the arena.
