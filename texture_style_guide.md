When we make textures for the server, we use the Zughy_32 palate 
https://lospec.com/palette-list/zughy-32

We only use these colors on the server, if at all possible.

Typically, if you are making a humaniod skin, you should start with the default skins found in skins_collectible and edit it to your needs.
