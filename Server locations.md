Minetest files can be found at

```
/home/minetest/minetest4/
```

minetest.conf
```
/home/minetest/minetest4/minetest.conf
```
==============================================

The executable command is 

```
/home/minetest/minetest4/bin/minetestserver
```
Important: you must be *in* the pwd of /home/minetest/minetest4 to run the server. Use tmux or screen to have other terminals.


==============================================


The world files are in  
```
/home/minetest/minetest4/worlds/pineapple_server_3/
```
==============================================


The game files are in 

```
/home/minetest/minetest4/games/aes_game
```

Do not edit the game files directly! They are all stored in the gitlab repo:
https://gitlab.com/arcadeemulationserver/aes_game

All changes should be commited to that repo, and pulled from the server. See "updating the game"

==============================================


