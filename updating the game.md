
first, on your own computer, clone the repo https://gitlab.com/arcadeemulationserver/aes_game using the recursive option so you can edit the files locally.

```
git clone https://gitlab.com/arcadeemulationserver/aes_game --recursive
```
This will also download the submodules (mods) that are linked in the main game repo.

++++
 OR
++++

If you already have the aes_game downloaded with git, then you should pull it instead of cloning it.

```
cd <path_to_aes_game>

git pull

git submodule update

```

that will pull the latest game files and mod files (submodules)

=====================================

Editing Mods (that we own in our group)

=====================================

1) cd into the mod that you want to edit

2) checkout the branch "master" (or "main", occasionally!) using the command line, in the mod directory

```
git checkout master
git pull
```

3) Open the files you want to edit and edit them, or add files, or remove files as you like

4) Go back to the command line, make sure you are in the mod directory (cd into it)

5) add all changes to the commit

```
git add -A
```
6) make your commit, and label it with a message

```
git commit -m "your message here"
```

7) push the commit to the online repo

``` 
git push
```
this will ask you for your gitlab username and password

8) cd into the upper aes_game repo

```
cd ..
```
(moves you up one folder)

9) to make the main game repo pull the submodule at the correct commit, you now have to add the changes to the main repo

(in the main aes_game repo)

```
git add -A
git commit -m "your commit message here"
git push
```
this will ask you for your gitlab username and password




==========================================

Adding submodules (new mods)

==========================================

Make sure aes_game is up-to-date on your local copy


we typically should have a copy of the repo under our gitlab group. Go to our Gitlab group:
https://gitlab.com/arcadeemulationserver

Click New Project

If we are cloning an existing mod, then click "import project/repository" 

Click "Repo by URL"

Paste the URL of the existing repo

If we will be editing the mod ourselves, then make sure the checkbox "Mirror Repository" IS NOT CHECKED

If we will just be using someone else's mod *without* editing it ourselves, then make sure that "Mirror Repository" IS CHECKED

It is better to Mirror the repository at first, because we can always stop mirroring in settings.

Make sure the project is "public" (its the last option)



Go ahead and make the project and COPY its repo location (the web address)

For example, the repo location of Arena_lib is https://gitlab.com/arcadeemulationserver/arena_lib

Yours will be different.


Then go to your terminal on your computer, cd into aes_game, make sure its up-to-date, and then run:

```
git submodule add <path-to-the repo-you copied> mods/<modname>
```

That will put the mod in a submodule folder in aes_game/mods/<modname> 

Then you have to add the change to the main repo

```
git add -A
git commit -m "your commit message here"
git push
```

==================================================

Updating the server

==================================================

After you have made changes to the game files, and uploaded them to the online repo, you can update the game files.

on the server, 

```
cd /home/minetest/minetest4/games/aes_game

git pull

git submodule update
```


