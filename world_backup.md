
The world files are in  
```
/home/minetest/minetest4/worlds/pineapple_server_3/
```

Every so often (once a week or so, unless no world changes have occurred), we shutdown the minetest program, and run the following commands:

```
cd /home/minetest/minetest4/worlds/

rm -rf pineapple_server_3_backup_2

cp pineapple_server_3_backup_1 pineapple_server_3_backup_2

rm -rf pineapple_server_3_backup_1

cp pineapple_server_3 pineapple_server_3_backup_1

```
We should also backup the world off-server occasionally
