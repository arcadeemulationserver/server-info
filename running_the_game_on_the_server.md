Read 
https://www.howtoforge.com/sharing-terminal-sessions-with-tmux-and-screen#sharing-terminal-sessions-between-two-different-accounts

We will always follow those instructions to create a sharable tmux session to run the server in. That way, any one of us can log on and view the server process and restart it if neccassary. We will use the group "minetestgroup", the socket "shareds", and the sessionname "shared". Try to connect to the session "shared" if that does not work, then create the session "shared".


**To attach to the session, if it is already running (always try this first!)** do:

```
tmux -S /home/minetestgroup/shareds attach -t shared
```


**To start a new session to run the server in**, do:


1)
```
$ su minetestuser
```
Note, you will need the minetestuser password, ask MisterE for it

2)
```
$ tmux -S /home/minetestgroup/shareds new -s shared
```
3)
```
$ chgrp minetestgroup /home/minetestgroup/shareds
```
4)
```
$ chmod g+rwx /home/minetestgroup/shareds
```

Note that to run the game, the path of working directory (the file location you are in, aka pwd) must be minetest's home folder
```
$ cd /home/minetest/minetest4/
```

